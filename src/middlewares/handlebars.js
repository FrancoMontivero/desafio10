"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var express_handlebars_1 = __importDefault(require("express-handlebars"));
var hbs = express_handlebars_1.default.create({ extname: "hbs" });
exports.default = hbs;

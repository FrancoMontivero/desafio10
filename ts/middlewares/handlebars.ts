import exphbs from 'express-handlebars';

let hbs = exphbs.create({extname: "hbs"});

export default hbs;

import express from 'express';

import path from 'path';
import cors from './middlewares/cors';
import router from './routes/index';
import morgan from './middlewares/morgan'
import multer from './middlewares/multer';
import handlebars from './middlewares/handlebars';

import { PORT } from './config';

const app = express();

app.use(express.json());
app.use(multer.array('any'));
app.engine('hbs', handlebars.engine);
app.use(express.urlencoded({ extended : true }));
app.use(express.static(path.resolve(__dirname, '../assets')));

app.use(cors);
app.use(morgan);

app.set('view engine', 'hbs')
app.set('views', path.resolve(__dirname, '../', 'views'));

app.use(router);
app.use((err: any, req: any, res: any, next: any) => {
    if (res.headersSent) {
        return next(err);
    }
    res.status(500);
    res.json({'error': err.message});
});

app.set('port', PORT);

export default app;